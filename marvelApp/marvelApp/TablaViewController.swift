//
//  TablaViewController.swift
//  marvelApp
//
//  Created by Robert on 29/11/17.
//  Copyright © 2017 Robert. All rights reserved.
//
import UIKit

class TablaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int { //numero de secciones que tendra
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { //numero de filas de cada seccion
        /*switch section {
        case 0:
            return 5
        default:
            return 10
        }*/
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { //nos devuelve las celdas
        let  cell = UITableViewCell()
        cell.textLabel?.text = "\(indexPath)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        /*switch section {
        case 0:
            return "seccion 1"
        default:
            return "seccion 2"
        }*/
        return "Ligas Europeas"
    }

}
