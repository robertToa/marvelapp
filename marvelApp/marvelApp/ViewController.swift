//
//  ViewController.swift
//  marvelApp
//
//  Created by Robert on 28/11/17.
//  Copyright © 2017 Robert. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLable: UILabel!
    @IBOutlet weak var heighLabel: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func consultarButton(_ sender: Any) {
        
       Alamofire.request("https://pokeapi.co/api/v2/pokemon/1").responseJSON { response in
            debugPrint(response)
            if let json = response.result.value{
                print("JSON: \(json)")
            }
        }
        
        let URL = "https://pokeapi.co/api/v2/pokemon/1"
        Alamofire.request(URL).responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            
            /*if let forecastArray = forecastArray {
                for forecast in forecastArray {
                    print(forecast.day)
                    print(forecast.temperature)
                }
            }*/
            print(pokemon?.name)
            print(pokemon?.height)
            print(pokemon?.weight)
            DispatchQueue.main.async {
                self.nameLabel.text = pokemon?.name ?? ""
                self.heighLabel.text = "\(pokemon?.height ?? 0)"
                self.weightLable.text = "\(pokemon?.weight ?? 0)"
                //self.descriptionTextField.text = pokemon?.height ?? 0
                
            }
            
        }
    }
    
    
    

}

